// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package filter

import (
	"strings"
	"time"

	"github.com/danwakefield/fnmatch"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
)

func matchString(value, query string, glob, casefold bool) bool {
	if glob {
		flags := 0
		if casefold {
			flags |= fnmatch.FNM_CASEFOLD
		}
		return fnmatch.Match(query, value, flags)
	} else {
		if casefold {
			return strings.ToLower(query) == strings.ToLower(value)
		} else {
			return query == value
		}
	}
}

func mergeReqFilterReject(this model.MergeReqFilter) model.MergeReqFilter {
	log.Infof("Filter reject %p", this)
	return func(mreq model.MergeReq) bool {
		return !this(mreq)
	}
}

func mergeReqFilterBoth(this, that model.MergeReqFilter) model.MergeReqFilter {
	log.Infof("Filter both %p %p", this, that)
	return func(mreq model.MergeReq) bool {
		return this(mreq) && that(mreq)
	}
}

func mergeReqFilterEither(this, that model.MergeReqFilter) model.MergeReqFilter {
	log.Infof("Filter either %p %p", this, that)
	return func(mreq model.MergeReq) bool {
		return this(mreq) || that(mreq)
	}
}

func mergeReqFilterNickName(nickname string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter nickname %s glob=%t casefold=%t", nickname, glob, casefold)
	return func(mreq model.MergeReq) bool {
		return matchString(mreq.Repo.NickName, nickname, glob, casefold)
	}
}

func mergeReqFilterState(state model.MergeReqState) model.MergeReqFilter {
	log.Infof("Filter state %s", state)
	return func(mreq model.MergeReq) bool {
		return mreq.State == state
	}
}

func mergeReqFilterMetadataStatus(status model.MergeReqStatus) model.MergeReqFilter {
	log.Infof("Filter status %s", status)
	return func(mreq model.MergeReq) bool {
		return mreq.Metadata.Status == status
	}
}

func mergeReqFilterAge(dur time.Duration) model.MergeReqFilter {
	log.Infof("Filter age %s", dur)
	then := time.Now().Add(dur * -1)
	return func(mreq model.MergeReq) bool {
		return mreq.CreatedAt.After(then)
	}
}

func mergeReqFilterActivity(dur time.Duration) model.MergeReqFilter {
	log.Infof("Filter activity %s", dur)
	then := time.Now().Add(dur * -1)
	return func(mreq model.MergeReq) bool {
		return mreq.UpdatedAt.After(then)
	}
}

func mergeReqFilterSubmitterRealName(name string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter real name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq model.MergeReq) bool {
		return matchString(mreq.Submitter.RealName, name, glob, casefold)
	}
}

func mergeReqFilterSubmitterUserName(name string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter user name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq model.MergeReq) bool {
		return matchString(mreq.Submitter.UserName, name, glob, casefold)
	}
}

func mergeReqFilterLabels(label string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter label %s", label)
	return func(mreq model.MergeReq) bool {
		for _, mrlabel := range mreq.Labels {
			if matchString(mrlabel, label, glob, casefold) {
				return true
			}
		}
		return false
	}
}

func mergeReqFilterTargetBranch(branch string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter target branch %s glob=%t casefold=%t", branch, glob, casefold)
	return func(mreq model.MergeReq) bool {
		return matchString(mreq.TargetBranch, branch, glob, casefold)
	}
}

func mergeReqFilterWIP() model.MergeReqFilter {
	log.Infof("Filter WIP")
	return func(mreq model.MergeReq) bool {
		return mreq.WIP
	}
}

func mergeReqFilterApproved(op ComparisonOperator, count int) model.MergeReqFilter {
	log.Infof("Filter approved comp-op=%d count=%d", op, count)
	return func(mreq model.MergeReq) bool {
		switch op {
		case ComparisonOperatorEqual:
			return len(mreq.Approvals.ApprovedBy) == count
		case ComparisonOperatorLess:
			return len(mreq.Approvals.ApprovedBy) < count
		case ComparisonOperatorLessOrEqual:
			return len(mreq.Approvals.ApprovedBy) <= count
		case ComparisonOperatorGreater:
			return len(mreq.Approvals.ApprovedBy) > count
		case ComparisonOperatorGreaterOrEqual:
			return len(mreq.Approvals.ApprovedBy) >= count
		}
		return len(mreq.Approvals.ApprovedBy) == count
	}
}

func mergeReqFilterApproverRealName(name string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter approver real name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq model.MergeReq) bool {
		match := false
		for _, acct := range mreq.Approvals.ApprovedBy {
			if matchString(acct.RealName, name, glob, casefold) {
				match = true
			}
		}
		return match
	}
}

func mergeReqFilterApproverUserName(name string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter approver user name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq model.MergeReq) bool {
		match := false
		for _, acct := range mreq.Approvals.ApprovedBy {
			if matchString(acct.UserName, name, glob, casefold) {
				match = true
			}
		}
		return match
	}
}

func mergeReqFilterReviewerUserName(name string, glob, casefold bool) model.MergeReqFilter {
	log.Infof("Filter reviewer user name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq model.MergeReq) bool {
		match := false
		for _, acct := range mreq.Reviewers {
			if matchString(acct.UserName, name, glob, casefold) {
				match = true
			}
		}
		return match
	}
}

func mergeReqFilterID(project string, id int) model.MergeReqFilter {
	log.Infof("Filter project ID %s#%d", project, id)
	return func(mreq model.MergeReq) bool {
		if project != "" && mreq.Repo.NickName != project {
			return false
		}
		return mreq.ID == uint(id)
	}

}

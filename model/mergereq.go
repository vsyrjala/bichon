// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type MergeReqState string

const (
	STATE_OPENED MergeReqState = "opened"
	STATE_CLOSED MergeReqState = "closed"
	STATE_MERGED MergeReqState = "merged"
	STATE_LOCKED MergeReqState = "locked"
)

type MergeReqStatus string

const (
	STATUS_NEW     MergeReqStatus = "new"
	STATUS_UPDATED MergeReqStatus = "updated"
	STATUS_OLD     MergeReqStatus = "old"
	STATUS_READ    MergeReqStatus = "read"
)

type MergeReq struct {
	Repo               Repo            `json:"-"`
	ID                 uint            `json:"id"`
	Title              string          `json:"title"`
	CreatedAt          time.Time       `json:"createdAt"`
	UpdatedAt          time.Time       `json:"updatedAt"`
	Submitter          Account         `json:"submitter"`
	Assignee           *Account        `json:"assignee"`
	Reviewers          []Account       `json:"reviewers"`
	Description        string          `json:"description"`
	Versions           []Series        `json:"versions"`
	Threads            []CommentThread `json:"threads"`
	State              MergeReqState   `json:"state"`
	Labels             []string        `json:"labels"`
	UpVotes            int             `json:"upVotes"`
	DownVotes          int             `json:"downVotes"`
	MergeStatus        string          `json:"mergeStatus"`
	MergeAfterPipeline bool            `json:"mergeAfterPipeline"`
	SourceBranch       string          `json:"sourceBranch"`
	TargetBranch       string          `json:"targetBranch"`
	WIP                bool            `json:"wip"`
	Approvals          Approvals       `json:"approvals"`

	Metadata MergeReqMetadata `json:"bichonMetadata"`
}

type MergeReqMetadata struct {
	Partial bool           `json:"partial"`
	Status  MergeReqStatus `json:"status"`
}

func (mreq *MergeReq) String() string {
	return fmt.Sprintf("%s#%d", mreq.Repo.String(), mreq.ID)
}

func (mreq *MergeReq) Equal(othermreq *MergeReq) bool {
	return mreq.Repo.Equal(&othermreq.Repo) && mreq.ID == othermreq.ID
}

func (mreq *MergeReq) Age() string {
	age := time.Since(mreq.CreatedAt)
	return formatDuration(age)
}

func (mreq *MergeReq) Activity() string {
	age := time.Since(mreq.UpdatedAt)
	return formatDuration(age)
}

func (mreq *MergeReq) ToJSON() ([]byte, error) {
	return json.MarshalIndent(mreq, "", "  ")
}

func NewMergeReqFromJSON(data []byte) (*MergeReq, error) {
	mreq := &MergeReq{}
	err := json.Unmarshal(data, mreq)
	if err != nil {
		return nil, err
	}
	return mreq, nil
}

func (mreq *MergeReq) SourceURL() string {
	groupproject := mreq.Repo.Project
	project := strings.Split(groupproject, "/")[1]

	return fmt.Sprintf("https://%s/%s/%s.git", mreq.Repo.Server, mreq.Submitter.UserName, project)
}

func (mreq *MergeReq) OriginRef() string {
	return fmt.Sprintf("merge-requests/%d/head", mreq.ID)
}

func (mreq *MergeReq) LocalBranch(version int) string {
	if version == 0 {
		version = len(mreq.Versions)
	}
	return fmt.Sprintf("mreq-%d-%s-v%d", mreq.ID, mreq.Submitter.UserName, version)
}

func (mreq *MergeReq) ReviewURL() string {
	return fmt.Sprintf("https://%s/%s/-/merge_requests/%d", mreq.Repo.Server, mreq.Repo.Project, mreq.ID)
}

func (mreq *MergeReq) ThreadIndexFromID(id string) (int, error) {
	for idx, thread := range mreq.Threads {
		if thread.ID == id {
			return idx, nil
		}
	}

	return -1, fmt.Errorf("No thread %s#%s", mreq.String(), id)
}

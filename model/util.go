// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"math"
	"time"
)

func formatDuration(dur time.Duration) string {
	min := int(dur.Round(time.Minute).Minutes())
	hour := int(dur.Round(time.Hour).Hours())
	days := int(math.Round(dur.Hours() / 24))
	weeks := int(math.Round(dur.Hours() / (24 * 7)))
	months := int(math.Round(dur.Hours() / (24 * 31)))
	years := int(math.Round(dur.Hours() / (24 * 365)))

	if dur.Minutes() <= 1 {
		return "just now"
	} else if min == 1 {
		return "1 min"
	} else if dur.Minutes() < 60 {
		return fmt.Sprintf("%d mins", min)
	} else if hour == 1 {
		return "1 hour"
	} else if dur.Hours() < 24 {
		return fmt.Sprintf("%d hours", int(dur.Round(time.Hour).Hours()))
	} else if days == 1 {
		return "1 day"
	} else if days < 7 {
		return fmt.Sprintf("%d days", days)
	} else if weeks == 1 {
		return "1 week"
	} else if days < 31 {
		return fmt.Sprintf("%d weeks", weeks)
	} else if months == 1 {
		return "1 mon"
	} else if months < 24 {
		return fmt.Sprintf("%d mons", months)
	} else {
		return fmt.Sprintf("%d years", years)
	}
}

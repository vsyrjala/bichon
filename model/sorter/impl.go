// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, package.

package sorter

import (
	"strings"

	"gitlab.com/bichon-project/bichon/model"
)

func mergeReqSorterID(a, b model.MergeReq) bool {
	return a.ID < b.ID
}

func mergeReqSorterTitle(a, b model.MergeReq) bool {
	return strings.ToLower(a.Title) < strings.ToLower(b.Title)
}

func mergeReqSorterReverse(this model.MergeReqSorter) model.MergeReqSorter {
	return func(a, b model.MergeReq) bool {
		return !this(a, b)
	}
}

func mergeReqSorterBoth(this, that model.MergeReqSorter) model.MergeReqSorter {
	return func(a, b model.MergeReq) bool {
		if this(a, b) {
			return true
		}
		if this(b, a) {
			return false
		}
		return that(a, b)
	}
}

func mergeReqSorterAge(a, b model.MergeReq) bool {
	return a.CreatedAt.After(b.CreatedAt)
}

func mergeReqSorterActivity(a, b model.MergeReq) bool {
	return a.UpdatedAt.After(b.UpdatedAt)
}

func mergeReqSorterRealName(a, b model.MergeReq) bool {
	return strings.ToLower(a.Submitter.RealName) < strings.ToLower(b.Submitter.RealName)
}

func mergeReqSorterUserName(a, b model.MergeReq) bool {
	return strings.ToLower(a.Submitter.UserName) < strings.ToLower(b.Submitter.UserName)
}

func mergeReqSorterProject(a, b model.MergeReq) bool {
	return a.Repo.NickName < b.Repo.NickName
}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019-2021 Red Hat, Inc.

package sorter

import (
	"fmt"

	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer"
	"github.com/alecthomas/participle/v2/lexer/stateful"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
)

type Name int

const (
	NameID Name = iota
	NameAge
	NameActivity
	NameTitle
	NameRealName
	NameUserName
	NameProject
)

var nameMap = map[string]Name{
	"id":       NameID,
	"age":      NameAge,
	"activity": NameActivity,
	"title":    NameTitle,
	"realname": NameRealName,
	"username": NameUserName,
	"project":  NameProject,
}

func (n *Name) Capture(str []string) error {
	val, ok := nameMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown field name '%s'", str[0])
	}
	*n = val
	return nil
}

func (n *Name) BuildSorter() model.MergeReqSorter {
	switch *n {
	case NameID:
		return mergeReqSorterID
	case NameAge:
		return mergeReqSorterAge
	case NameActivity:
		return mergeReqSorterActivity
	case NameTitle:
		return mergeReqSorterTitle
	case NameRealName:
		return mergeReqSorterRealName
	case NameUserName:
		return mergeReqSorterUserName
	case NameProject:
		return mergeReqSorterProject
	}

	panic("unknown sort field name")
}

//  Expression: Field (, Field ) *
//  Field: '!'? Name

type Expression struct {
	Left  *Field   `@@`
	Right []*Field `("," @@)*`
}

type Field struct {
	Reverse bool `@("!")?`
	Name    Name `@Name`
}

func (f *Field) BuildSorter() model.MergeReqSorter {
	if f.Reverse {
		return mergeReqSorterReverse(f.Name.BuildSorter())
	} else {
		return f.Name.BuildSorter()
	}
}

func (e *Expression) BuildSorter() model.MergeReqSorter {
	left := e.Left.BuildSorter()
	for _, right := range e.Right {
		left = mergeReqSorterBoth(left,
			right.BuildSorter())
	}
	return left
}

func NewExpression(sorter string) (*Expression, error) {
	lxr := lexer.Must(stateful.NewSimple([]stateful.Rule{
		{"Punct", `[!,]`, nil},
		{"Name", `[a-zA-Z-]+`, nil},
		{"Whitespace", `[ \t\n\r]+`, nil},
	}))
	p := participle.MustBuild(&Expression{},
		participle.Lexer(lxr),
		participle.Elide("Whitespace"),
	)

	log.Infof("Parsing sorter expression '%s'", sorter)
	expr := &Expression{}
	err := p.ParseString("", sorter, expr)
	if err != nil {
		log.Infof("Error in sorter expression: %s", err)
		return nil, err
	}
	return expr, nil
}

type SorterInfo struct {
	Expression string
	Comparator model.MergeReqSorter
	Error      error
}

func BuildSorter(sorterexpr string) SorterInfo {
	expr, err := NewExpression(sorterexpr)

	sorterinfo := SorterInfo{
		Expression: sorterexpr,
		Error:      err,
	}

	if err == nil {
		sorterinfo.Comparator = expr.BuildSorter()
	}

	return sorterinfo
}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"strings"

	"github.com/satori/go.uuid"
)

type Report struct {
	ID      string
	Name    string
	Filter  string
	Sorter  string
	Default bool
	Active  bool
}

func NewReport(name, filter, sorter string, def bool) Report {
	return Report{
		ID:      uuid.NewV4().String(),
		Name:    name,
		Filter:  filter,
		Sorter:  sorter,
		Default: def,
		Active:  false,
	}
}

type Reports []Report

func (r Reports) Len() int {
	return len(r)
}

func (r Reports) Less(i, j int) bool {
	return strings.ToLower(r[i].Name) < strings.ToLower(r[j].Name)
}

func (r Reports) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r Reports) IndexOfReport(report Report) int {
	for idx, thisreport := range r {
		if report.ID == thisreport.ID {
			return idx
		}
	}
	return -1
}

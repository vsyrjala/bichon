// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

type Comment struct {
	Author      User      `json:"author"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedt"`
	Description string    `json:"description"`
	System      bool      `json:"system"`
	Resolvable  bool      `json:"resolvable"`
	Resolved    bool      `json:"resolved"`
	Resolver    *Account  `json:"resolver"`

	Context *CommentContext `json:"context"`
}

type CommentThread struct {
	ID         string    `json:"id"`
	Individual bool      `json:"individual"`
	Comments   []Comment `json:"comments"`
}

type CommentContext struct {
	BaseHash  string `json:"baseHash"`
	StartHash string `json:"startHash"`
	HeadHash  string `json:"headHash"`
	NewFile   string `json:"newFile"`
	NewLine   uint   `json:"newLine"`
	OldFile   string `json:"oldFile"`
	OldLine   uint   `json:"oldLine"`
}

func (ctx *CommentContext) String() string {
	return fmt.Sprintf("start=%s head=%s %s:%d %s:%d",
		ctx.StartHash, ctx.HeadHash,
		ctx.OldFile, ctx.OldLine, ctx.NewFile, ctx.NewLine)
}

func (ctx *CommentContext) Rebase(target *Commit, series *Series) *CommentContext {
	log.Infof("Rebasing %s to %s", ctx.String(), target.Hash)

	newCtx := *ctx

	headPatch := -1
	startPatch := 0
	targetPatch := -1
	for idx, commit := range series.Patches {
		if newCtx.HeadHash == commit.Hash {
			headPatch = idx
		}
		if newCtx.StartHash == commit.Hash {
			startPatch = idx + 1
		}
		if target.Hash == commit.Hash {
			targetPatch = idx
		}
	}

	log.Infof("Identified head %d (%s) start %d (%s) target %d (%s)",
		headPatch+1, newCtx.HeadHash, startPatch+1, newCtx.StartHash, targetPatch+1, target.Hash)
	if headPatch == -1 {
		log.Infof("Head commit %s is not present in series", newCtx.HeadHash)
		return nil
	}
	if targetPatch == -1 {
		log.Infof("Target commit %s is not present in series", target.Hash)
		return nil
	}

	if headPatch < targetPatch {
		log.Infof("Comment was on head %d/%d (%s) before the target patch %d/%d",
			headPatch, len(series.Patches), newCtx.HeadHash, targetPatch, len(series.Patches))
		return nil
	}

	if startPatch > targetPatch {
		log.Infof("Comment was on start %d/%d (%s) after the target patch %d/%d",
			startPatch, len(series.Patches), newCtx.StartHash, targetPatch, len(series.Patches))
		return nil
	}

	// Added:  NewLine != 0, OldLine == 0
	// Removed:  NewLine == 0, OldLine != 0
	// Context: NewLine != 0, OldLine != 0
	//
	// If NewLine is non-zero, then we work backwards
	// from the HeadHash
	//
	// If OldLine is non-zero, then we work forwards
	// from the BaseHash

	if newCtx.NewLine != 0 {
		log.Infof("Revert to update new line")
		for i := headPatch; i >= targetPatch; i-- {
			commit := series.Patches[i]
			log.Infof("Reverting patch %d %s", i+1, commit.Hash)
			diff := commit.GetFile(newCtx.NewFile)
			if diff == nil {
				log.Infof("File %s is not in commit", newCtx.NewFile)
				continue
			}

			if diff.IsBinarySummary() {
				log.Infof("File %s is just a binary diff summary", newCtx.NewFile)
				continue
			}

			location, origLine, err := diff.Revert(newCtx.NewLine)
			log.Infof("Reverted patch %d %s  %d -> %d", i+1, commit.Hash, newCtx.NewLine, origLine)
			if err != nil {
				log.Infof("Failed to revert %s", err)
				return nil
			}
			if location == DIFF_LINE_LOCATION_REMOVED {
				log.Info("Impossible line location removed")
				return nil
			}
			if i == targetPatch {
				if newCtx.OldLine == 0 {
					if location != DIFF_LINE_LOCATION_ADDED {
						log.Info("Discarding comment at end, expected add, got context/outside")
						return nil
					}
				} else {
					if location == DIFF_LINE_LOCATION_ADDED {
						log.Info("Discarding comment at end, expected context, got add")
						return nil
					}
					if location == DIFF_LINE_LOCATION_OUTSIDE {
						log.Info("Discarding comment at end, expected context, got outside")
						return nil
					}
				}
			} else {
				if location == DIFF_LINE_LOCATION_ADDED {
					log.Infof("Discarding comment at patch %d, expected context/outside, got add", i)
					return nil
				}
				newCtx.NewLine = origLine
				newCtx.HeadHash = series.Patches[i].Hash
				if diff.RenamedFile {
					log.Infof("Renamed from %s to %s", newCtx.NewFile, diff.OldFile)
					newCtx.NewFile = diff.OldFile
				}
			}
		}

	}
	if newCtx.OldLine != 0 {
		log.Infof("Apply to update old line")
		for i := startPatch; i <= targetPatch; i++ {
			commit := series.Patches[i]
			log.Infof("Applying patch %d %s", i+1, commit.Hash)
			diff := commit.GetFile(newCtx.OldFile)
			if diff == nil {
				log.Infof("File %s is not in commit", newCtx.OldFile)
				continue
			}

			if diff.IsBinarySummary() {
				log.Infof("File %s is just a binary diff summary", newCtx.NewFile)
				continue
			}

			location, newLine, err := diff.Apply(newCtx.OldLine)
			log.Infof("Applied patch %d %s  %d -> %d", i+1, commit.Hash, newCtx.OldLine, newLine)
			if err != nil {
				log.Infof("Failed to apply %s", err)
				return nil
			}
			if location == DIFF_LINE_LOCATION_ADDED {
				log.Info("Impossible line location added")
				return nil
			}
			if i == targetPatch {
				if newCtx.NewLine == 0 {
					if location != DIFF_LINE_LOCATION_REMOVED {
						log.Info("Discarding comment at end, expected remove, got context/outside")
						return nil
					}
				} else {
					if location == DIFF_LINE_LOCATION_REMOVED {
						log.Info("Discarding comment at end, expected context, got remove")
						return nil
					}
					if location == DIFF_LINE_LOCATION_OUTSIDE {
						log.Info("Discarding comment at end, expected context, got outside")
						return nil
					}
				}
			} else {
				if location == DIFF_LINE_LOCATION_REMOVED {
					log.Infof("Discarding comment at patch %d, expected context/outside, got remove", i)
					return nil
				}
				newCtx.OldLine = newLine
				newCtx.StartHash = series.Patches[i].Hash
				if diff.RenamedFile {
					log.Infof("Renamed from %s to %s", newCtx.OldFile, diff.NewFile)
					newCtx.OldFile = diff.NewFile
				}
			}
		}
	}

	log.Infof("Rebased %s to %s", newCtx.String(), target.Hash)

	return &newCtx
}

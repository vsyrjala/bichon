// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package view

import (
	"fmt"
	"testing"
)

func TestTrim(t *testing.T) {
	type TestData struct {
		Input  string
		Output string
		Length int
	}

	data := []TestData{
		{"Daniel Berrange", "Daniel Berrange", 20},
		{"Daniel Berrange", "…Berrange", 9},
		// chomps the whitespace
		{"Daniel Berrange", "…Berrange", 10},
		{"Neal Gompa (ニール・ゴンパ)", "…pa (ニール・ゴンパ)", 20},
		{"Neal Gompa (ニール・ゴンパ)", "…(ニール・ゴンパ)", 17},
		// chomps the whitespace
		{"Neal Gompa (ニール・ゴンパ)", "…(ニール・ゴンパ)", 18},
		{"Neal Gompa (ニール・ゴンパ)", "…・ゴンパ)", 10},
	}

	for _, entry := range data {
		actual := TrimEllipsisFront(entry.Input, entry.Length)
		if actual != entry.Output {
			t.Fatalf("Trim '%s' to %d, expected '%s' but got '%s'",
				entry.Input, entry.Length, entry.Output, actual)
		}
	}
}

func TestHighlightTrailingSpace(t *testing.T) {
	colorWhitespace := fmt.Sprintf("[:%s:]",
		GetStyleColorName(ELEMENT_DIFF_WHITESPACE_FILL))

	type TestData struct {
		Input  string
		Output string
		Offset uint
	}

	data := []TestData{
		{"", "", 0},
		{" ", colorWhitespace + " ", 0},
		{" fish", " fish", 0},
		{" fish ", " fish" + colorWhitespace + " ", 0},
		{"Hello World \u200a ", "Hello World" + colorWhitespace + " \u200a ", 0},
		{" ", " ", 1},
		{"  ", " " + colorWhitespace + " ", 1},
	}

	for idx, entry := range data {
		got := HighlightTrailingSpace(entry.Input, colorWhitespace, entry.Offset)
		if got != entry.Output {
			t.Fatalf("Highlight whitepace %d got '%s' expected '%s'",
				idx, got, entry.Output)
		}
	}
}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package view

import (
	"fmt"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type CommentFormListener interface {
	CommentFormSave(text string, standAlone bool)
	CommentFormCancel()
}

type CommentForm struct {
	*tview.Form

	Message     *tview.TextView
	Text        *tview.TextArea
	Listener    CommentFormListener
	StartThread *tview.Checkbox

	Context *model.CommentContext
}

func NewCommentForm(listener CommentFormListener) *CommentForm {
	form := &CommentForm{
		Form: tview.NewForm(),

		Listener: listener,
	}

	form.SetBorder(true).SetTitle("Add comment")

	form.Message = addTextView(form.Form, "", "", 0, 1, false)
	form.Text = addTextArea(form.Form, "", "", 0, 10, false)
	form.StartThread = addCheckbox(form.Form, "", "Enable replies to comment", true, false)

	form.Form.AddDefaultButton("Save", form.buttonSave)
	form.Form.AddButton("Cancel", form.buttonCancel)

	return form
}

func (form *CommentForm) buttonSave() {
	text := form.Text.GetText()
	standAlone := !form.StartThread.IsChecked()
	form.Listener.CommentFormSave(text, standAlone)
	form.Text.SetText("", true)
}

func (form *CommentForm) buttonCancel() {
	form.Listener.CommentFormCancel()
}

func (form *CommentForm) Activate(app *tview.Application, context *model.CommentContext, reply int, allowStandalone bool) {
	form.Text.SetText("", true)
	app.SetFocus(form.Form)
	form.Form.SetFocus(0)

	if allowStandalone {
		form.StartThread.SetChecked(false)
		form.StartThread.SetDisabled(false)
	} else {
		form.StartThread.SetChecked(true)
		form.StartThread.SetDisabled(true)
	}

	if context != nil {
		if reply != -1 {
			form.SetTitle(fmt.Sprintf("Reply to commit diff comment #%d", reply+1))
		} else {
			form.SetTitle("Add commit diff comment")
		}
		var msg string
		if context.OldFile != "" && context.OldLine != 0 {
			msg += fmt.Sprintf("old file %s:%d", context.OldFile, context.OldLine)
		}
		if context.NewFile != "" && context.NewLine != 0 {
			if msg != "" {
				msg += ", "
			}
			msg += fmt.Sprintf("new file %s:%d", context.NewFile, context.NewLine)
		}
		form.Message.SetText("Commenting at " + msg)
	} else {
		if reply != -1 {
			form.SetTitle(fmt.Sprintf("Reply to merge request comment #%d", reply+1))
		} else {
			form.SetTitle("Add merge request comment")
		}
		form.Message.SetText("Commenting on cover letter")
	}
}

func (form *CommentForm) IsEditFocused() bool {
	return form.Text.HasFocus()
}

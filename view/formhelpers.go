// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019-2020 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/tview"
)

func addTextView(form *tview.Form, label, value string, width, height int, readonly bool) *tview.TextView {
	form.AddTextView(label, value, width, height, false, false)

	item := form.GetFormItem(form.GetFormItemCount() - 1)

	textview, _ := item.(*tview.TextView)

	textview.SetDisabled(readonly)

	return textview
}

func addTextArea(form *tview.Form, label, value string, width, height int, readonly bool) *tview.TextArea {
	form.AddTextArea(label, value, width, height, 0, nil)

	item := form.GetFormItem(form.GetFormItemCount() - 1)

	textarea, _ := item.(*tview.TextArea)

	textarea.SetDisabled(readonly)

	return textarea
}

func addInputField(form *tview.Form, label, value string, width int, readonly bool) *tview.InputField {
	form.AddInputField(label, value, width, nil, nil)

	item := form.GetFormItem(form.GetFormItemCount() - 1)

	input, _ := item.(*tview.InputField)

	input.SetDisabled(readonly)

	return input
}

func addCheckbox(form *tview.Form, label string, message string, value bool, readonly bool) *tview.Checkbox {
	form.AddCheckbox(label, value, nil)

	item := form.GetFormItem(form.GetFormItemCount() - 1)

	input, _ := item.(*tview.Checkbox)

	input.SetDisabled(readonly)
	input.SetMessage(message)

	return input
}

func addDropDown(form *tview.Form, label string, options []string, readonly bool) *tview.DropDown {
	form.AddDropDown(label, options, 0, nil)

	item := form.GetFormItem(form.GetFormItemCount() - 1)

	input, _ := item.(*tview.DropDown)

	input.SetDisabled(readonly)

	return input
}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/analysis"
	"gitlab.com/bichon-project/bichon/diff"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type PatchRegion struct {
	IDStart   int
	IDEnd     int
	Type      diff.DiffLineType
	OldFile   string
	OldLine   uint
	NewFile   string
	NewLine   uint
	Thread    string
	ThreadIdx int
}

func formatLine(text, prefix, color string, region *int) string {
	*region++
	colorDefault := fmt.Sprintf("[%s:%s:-]",
		GetStyleColorName(ELEMENT_PRIMARY_TEXT),
		GetStyleColorName(ELEMENT_PRIMITIVE_FILL))

	return fmt.Sprintf("%s[\"l%d\"]%s[\"\"]%s%s%s", colorDefault, *region, prefix, color, text, colorDefault)
}

func formatHunks(oldFile, newFile, commit string, region *int, commentShow bool, threadsmap map[string][]int, threads []model.CommentThread, reportsMap map[string][]string) ([]string, []PatchRegion, error) {
	colorDefault := fmt.Sprintf("[%s:%s:-]",
		GetStyleColorName(ELEMENT_PRIMARY_TEXT),
		GetStyleColorName(ELEMENT_PRIMITIVE_FILL))
	colorHunk :=
		GetStyleMarker(
			ELEMENT_DIFF_HUNK_TEXT,
			ELEMENT_DIFF_HUNK_FILL,
			ELEMENT_DIFF_HUNK_ATTR)
	colorLine :=
		GetStyleMarker(
			ELEMENT_DIFF_LINE_TEXT,
			ELEMENT_DIFF_LINE_FILL,
			ELEMENT_DIFF_LINE_ATTR)
	colorAdded :=
		GetStyleMarker(
			ELEMENT_DIFF_ADDED_TEXT,
			ELEMENT_DIFF_ADDED_FILL,
			ELEMENT_DIFF_ADDED_ATTR)
	colorRemoved :=
		GetStyleMarker(
			ELEMENT_DIFF_REMOVED_TEXT,
			ELEMENT_DIFF_REMOVED_FILL,
			ELEMENT_DIFF_REMOVED_ATTR)
	colorContext :=
		GetStyleMarker(
			ELEMENT_DIFF_CONTEXT_TEXT,
			ELEMENT_DIFF_CONTEXT_FILL,
			ELEMENT_DIFF_CONTEXT_ATTR)
	colorAlert :=
		GetStyleMarker(
			ELEMENT_DIFF_ALERT_TEXT,
			ELEMENT_DIFF_ALERT_FILL,
			ELEMENT_DIFF_ALERT_ATTR)
	colorWhitespace := fmt.Sprintf("[:%s:]",
		GetStyleColorName(ELEMENT_DIFF_WHITESPACE_FILL))

	hunks, err := diff.ParseUnifiedDiffHunks(commit)
	if err != nil {
		return []string{}, []PatchRegion{}, err
	}

	lines := make([]string, 0)
	oldMax := uint(0)
	newMax := uint(0)
	for _, hunk := range hunks {
		for _, line := range hunk.Lines {
			if line.Type == diff.DIFF_LINE_ADDED ||
				line.Type == diff.DIFF_LINE_CONTEXT {
				newMax = line.NewLine
			}
			if line.Type == diff.DIFF_LINE_REMOVED ||
				line.Type == diff.DIFF_LINE_CONTEXT {
				oldMax = line.OldLine
			}
		}
	}
	oldDigits := len(fmt.Sprintf("%d", oldMax))
	newDigits := len(fmt.Sprintf("%d", newMax))

	fmtOldLine := fmt.Sprintf("%s%%%dd%s", colorLine, oldDigits, colorDefault)
	fmtNewLine := fmt.Sprintf("%s%%%dd%s", colorLine, newDigits, colorDefault)
	fmtOldPad := fmt.Sprintf("%%%ds", oldDigits)
	fmtNewPad := fmt.Sprintf("%%%ds", newDigits)

	regions := make([]PatchRegion, 0)
	prevType := diff.DiffLineType(-1)
	for _, hunk := range hunks {
		lines = append(lines, formatLine(hunk.FormatContext(), " ", colorHunk, region))

		for _, line := range hunk.Lines {
			var prefix string
			var color string

			var key string
			var threadidxs []int
			var reports []string
			if line.NewLine != 0 {
				key = fmt.Sprintf("new:%s:%d", newFile, line.NewLine)
				threadidxs = append(threadidxs, threadsmap[key]...)
				msg, _ := reportsMap[key]
				if len(msg) != 0 {
					reports = append(reports, msg...)
				}
			}
			if line.OldLine != 0 {
				key = fmt.Sprintf("old:%s:%d", oldFile, line.OldLine)
				threadidxs = append(threadidxs, threadsmap[key]...)
				msg, _ := reportsMap[key]
				if len(msg) != 0 {
					reports = append(reports, msg...)
				}
			}

			commentIcon := "  "
			if len(threadidxs) > 0 {
				commentIcon = GetStyleAltStr(OPTION_ALLOW_EMOJI, "📝", "> ")
			}
			if line.Type == diff.DIFF_LINE_CONTEXT {
				prefix = fmt.Sprintf(commentIcon+fmtOldLine+" "+fmtNewLine, line.OldLine, line.NewLine)
				color = colorContext
			} else if line.Type == diff.DIFF_LINE_REMOVED {
				prefix = fmt.Sprintf(commentIcon+fmtOldLine+" "+fmtNewPad, line.OldLine, "")
				color = colorRemoved
			} else if line.Type == diff.DIFF_LINE_ADDED {
				prefix = fmt.Sprintf(commentIcon+fmtOldPad+" "+fmtNewLine, "", line.NewLine)
				color = colorAdded
			}

			text := HighlightTrailingSpace(tview.Escape(line.Text), colorWhitespace, 1)
			text = analysis.ShowDanger(text, colorAlert, GetStyleOption(OPTION_ALLOW_EMOJI))

			lines = append(lines, formatLine(" "+text, prefix, color, region))

			var info *PatchRegion
			if line.Type != prevType {
				info = &PatchRegion{
					IDStart: *region,
					IDEnd:   *region,
					Type:    line.Type,
					OldFile: oldFile,
					NewFile: newFile,
					OldLine: uint(line.OldLine),
					NewLine: uint(line.NewLine),
				}
				prevType = line.Type
				regions = append(regions, *info)
			} else {
				info = &regions[len(regions)-1]
				info.IDEnd = *region
			}

			if len(threadidxs) > 0 && commentShow {
				indent := strings.Repeat(" ", newDigits+oldDigits+4) + "|"
				clines, cregions := FormatThreads(threads, threadidxs, false, true, region, indent)
				lines = append(lines, clines...)
				regions = append(regions, cregions...)
				prevType = diff.DiffLineType(-1)
			}
			if len(reports) > 0 {
				indent := strings.Repeat(" ", newDigits+oldDigits+4) + "|"
				clines := FormatReports(reports, indent)
				lines = append(lines, clines...)
				prevType = diff.DiffLineType(-1)
			}

		}
	}

	return lines, regions, nil
}

func formatDiff(diff *model.Diff, region *int, commentShow bool, threadsmap map[string][]int, threads []model.CommentThread, reportsMap map[string][]string) ([]string, []PatchRegion, error) {
	colorPreamble :=
		GetStyleMarker(
			ELEMENT_DIFF_PREAMBLE_TEXT,
			ELEMENT_DIFF_PREAMBLE_FILL,
			ELEMENT_DIFF_PREAMBLE_ATTR)
	oldFile := diff.OldFile
	newFile := diff.NewFile
	if oldFile == "" {
		oldFile = "/dev/null"
	}
	if newFile == "" {
		newFile = "/dev/null"
	}

	lines := []string{
		formatLine(fmt.Sprintf("diff a/%s b/%s", tview.Escape(oldFile), tview.Escape(newFile)),
			" ", colorPreamble, region),
	}

	var action string
	if diff.CreatedFile {
		action = fmt.Sprintf("new file mode %s", diff.NewMode)
	} else if diff.RenamedFile {
		action = fmt.Sprintf("renamed file mode %s - %s", diff.OldMode, diff.NewMode)
	} else if diff.DeletedFile {
		action = fmt.Sprintf("delete file mode %s", diff.OldMode)
	} else {
		action = fmt.Sprintf("file mode %s - %s", diff.OldMode, diff.NewMode)
	}
	lines = append(lines, formatLine(action, " ", colorPreamble, region))

	var regions []PatchRegion
	if diff.IsBinarySummary() {
		lines = append(lines, formatLine(diff.Content, " ", colorPreamble, region))
	} else {
		lines = append(lines,
			formatLine(fmt.Sprintf("--- a/%s", tview.Escape(oldFile)), " ", colorPreamble, region),
			formatLine(fmt.Sprintf("+++ b/%s", tview.Escape(newFile)), " ", colorPreamble, region))

		var hunks []string
		var err error
		hunks, regions, err = formatHunks(oldFile, newFile, diff.Content, region, commentShow, threadsmap, threads, reportsMap)
		if err != nil {
			return []string{}, []PatchRegion{}, err
		}
		lines = append(lines, hunks...)
	}

	return lines, regions, nil
}

func buildCommentMap(commit *model.Commit, series *model.Series, threads []model.CommentThread) (filethreads map[string][]int) {
	filethreads = make(map[string][]int)
	for idx, thread := range threads {
		log.Infof("Check thread %s", thread.ID)
		if len(thread.Comments) == 0 {
			log.Infof("No comments on thread %s", thread.ID)
			continue
		}
		if thread.Comments[0].Context == nil {
			log.Infof("No context on thread %s", thread.ID)
			continue
		}
		ctx := thread.Comments[0].Context.Rebase(commit, series)
		if ctx == nil {
			log.Infof("No rebased context on thread %s", thread.ID)
			continue
		}

		var key string
		var ok bool
		if ctx.NewLine != 0 {
			key = fmt.Sprintf("new:%s:%d", ctx.NewFile, ctx.NewLine)
		} else if ctx.OldLine != 0 {
			key = fmt.Sprintf("old:%s:%d", ctx.OldFile, ctx.OldLine)
		}
		if key != "" {
			_, ok = filethreads[key]
			if !ok {
				filethreads[key] = []int{}
			}
			filethreads[key] = append(filethreads[key], idx)
		}
	}
	return
}

func buildReportsMap(commit *model.Commit) map[string][]string {
	reports := analysis.AnalysePatch(commit, GetStyleOption(OPTION_ALLOW_EMOJI))

	reportsMap := make(map[string][]string)
	for idx, report := range reports {
		log.Infof("Check report %d", idx)
		var key string
		ctx := report.Context
		if ctx.NewFile != "" {
			key = fmt.Sprintf("new:%s:%d", ctx.NewFile, ctx.NewLine)
		} else if ctx.OldFile != "" {
			key = fmt.Sprintf("old:%s:%d", ctx.OldFile, ctx.OldLine)
		}
		if key != "" {
			_, ok := reportsMap[key]
			if !ok {
				reportsMap[key] = []string{}
			}
			reportsMap[key] = append(reportsMap[key], report.Message)
		}
	}
	return reportsMap
}

func FormatCommit(commit *model.Commit, series *model.Series, commentShow bool, threads []model.CommentThread) ([]string, []PatchRegion, error) {
	colorHeader :=
		GetStyleMarker(
			ELEMENT_SUMMARY_HEADER_TEXT,
			ELEMENT_SUMMARY_HEADER_FILL,
			ELEMENT_SUMMARY_HEADER_ATTR)
	colorDate :=
		GetStyleMarker(
			ELEMENT_SUMMARY_DATE_TEXT,
			ELEMENT_SUMMARY_DATE_FILL,
			ELEMENT_SUMMARY_DATE_ATTR)
	colorAuthor :=
		GetStyleMarker(
			ELEMENT_SUMMARY_AUTHOR_TEXT,
			ELEMENT_SUMMARY_AUTHOR_FILL,
			ELEMENT_SUMMARY_AUTHOR_ATTR)

	region := 0

	threadsmap := buildCommentMap(commit, series, threads)

	reports := buildReportsMap(commit)

	lines := []string{
		formatLine(fmt.Sprintf("   Commit: %s", commit.Hash), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("   Author: %s%s <%s>",
			colorAuthor,
			tview.Escape(commit.Author.Name),
			tview.Escape(commit.Author.Email)), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("  Created: %s%s",
			colorDate,
			commit.CreatedAt.Format(time.RFC1123)), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("Committer: %s%s <%s>",
			colorAuthor,
			tview.Escape(commit.Committer.Name),
			tview.Escape(commit.Committer.Email)), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("  Updated: %s%s",
			colorDate,
			commit.UpdatedAt.Format(time.RFC1123)), " ", colorHeader, &region),
		formatLine("", " ", colorHeader, &region),
	}

	for _, line := range strings.Split(commit.Message, "\n") {
		lines = append(lines, formatLine(tview.Escape(line), " ", "", &region))
	}

	log.Info("Creating diffstat")
	files := make(map[string]string)
	for _, diff := range commit.Diffs {
		files[diff.NewFile] = diff.Content
	}
	stat, err := diff.DiffStat(files)
	if err == nil {
		log.Info("Formatting diffstat")
		lines = append(lines, diff.FormatDiffStat(stat, 80)...)
		lines = append(lines, "")
	} else {
		log.Infof("Failed to parse diff %s", err)
	}

	var allregions []PatchRegion
	if commit.Metadata.Partial {
		lines = append(lines, formatLine("Please wait, patch diff is being loaded...", " ", "", &region))
	} else {
		for _, diff := range commit.Diffs {
			difflines, regions, err := formatDiff(&diff, &region, commentShow, threadsmap, threads, reports)
			if err != nil {
				return []string{}, []PatchRegion{}, err
			}
			lines = append(lines, difflines...)
			allregions = append(allregions, regions...)
		}
	}

	return lines, allregions, nil
}

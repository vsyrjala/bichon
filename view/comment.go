// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

func colorfyComment(data, indent string) (lines []string) {
	data = tview.Escape(data)
	colorDefault := fmt.Sprintf("[%s:%s:-]",
		GetStyleColorName(ELEMENT_PRIMARY_TEXT),
		GetStyleColorName(ELEMENT_PRIMITIVE_FILL))
	colorQuoted :=
		GetStyleMarker(
			ELEMENT_COMMENT_QUOTED_TEXT,
			ELEMENT_COMMENT_QUOTED_FILL,
			ELEMENT_COMMENT_QUOTED_ATTR)

	lines = strings.Split(data, "\n")
	for idx, _ := range lines {
		if len(lines[idx]) > 0 && lines[idx][0] == '>' {
			lines[idx] = colorQuoted + indent + lines[idx]
		} else {
			lines[idx] = colorDefault + indent + lines[idx]
		}
	}

	return lines
}

func FormatThreads(threads []model.CommentThread, threadidxs []int, filemarkers bool, endsep bool, region *int, indent string) (lines []string, regions []PatchRegion) {
	colorDefault := fmt.Sprintf("[%s:%s:-]",
		GetStyleColorName(ELEMENT_PRIMARY_TEXT),
		GetStyleColorName(ELEMENT_PRIMITIVE_FILL))
	colorSeparator :=
		GetStyleMarker(
			ELEMENT_SUMMARY_SEPARATOR_TEXT,
			ELEMENT_SUMMARY_SEPARATOR_FILL,
			ELEMENT_SUMMARY_SEPARATOR_ATTR)
	colorHeader :=
		GetStyleMarker(
			ELEMENT_SUMMARY_HEADER_TEXT,
			ELEMENT_SUMMARY_HEADER_FILL,
			ELEMENT_SUMMARY_HEADER_ATTR)
	colorDate :=
		GetStyleMarker(
			ELEMENT_SUMMARY_DATE_TEXT,
			ELEMENT_SUMMARY_DATE_FILL,
			ELEMENT_SUMMARY_DATE_ATTR)
	colorAuthor :=
		GetStyleMarker(
			ELEMENT_SUMMARY_AUTHOR_TEXT,
			ELEMENT_SUMMARY_AUTHOR_FILL,
			ELEMENT_SUMMARY_AUTHOR_ATTR)
	for _, idx := range threadidxs {
		thread := threads[idx]
		var tlines []string
		var ctxt *model.CommentContext

		sep := colorSeparator + "[$" + string(tview.BoxDrawingsHeavyHorizontal) + "]" + colorDefault
		*region++
		lines = append(lines, fmt.Sprintf("[\"l%d\"]%s[\"\"]%s", *region, indent, sep))

		for cidx, comment := range thread.Comments {
			indent := "[|]"

			sepstyle := tview.BoxDrawingsLightHorizontal
			if cidx == 0 {
				sepstyle = tview.BoxDrawingsHeavyHorizontal
			}
			if cidx > 0 {
				separator := colorSeparator + indent + "[$" + string(sepstyle) + "]" + colorDefault
				tlines = append(tlines, separator)
			}

			var header string
			if cidx == 0 {
				if thread.Individual {
					header = colorHeader + fmt.Sprintf("Comment #%d by ", idx+1)
				} else {
					header = colorHeader + fmt.Sprintf("Thread #%d by ", idx+1)
				}
			} else {
				indent = "   [|]"
				header = colorHeader + indent + "Reply by "
			}
			header += colorAuthor + tview.Escape(comment.Author.Name) +
				colorHeader + " on " +
				colorDate + comment.CreatedAt.Format(time.RFC1123) +
				colorDefault

			tlines = append(tlines, header)
			tlines = append(tlines, colorDefault+indent)

			if comment.Context != nil && cidx == 0 && filemarkers {
				ctx := comment.Context
				if ctx.NewFile != "" {
					tlines = append(tlines,
						fmt.Sprintf("File %s:%d (%s)", tview.Escape(ctx.NewFile), ctx.NewLine, ctx.HeadHash))
				} else {
					tlines = append(tlines,
						fmt.Sprintf("File %s:%d (%s)", tview.Escape(ctx.OldFile), ctx.OldLine, ctx.HeadHash))
				}
				tlines = append(tlines, indent)
			}
			desc := colorfyComment(comment.Description, indent)
			tlines = append(tlines, desc...)
			tlines = append(tlines, indent)
		}

		if len(thread.Comments) != 0 {
			comment := thread.Comments[0]
			ctxt = comment.Context
			if comment.Resolved {
				indent := "[|]"
				separator := colorSeparator + indent + "[$" + string(tview.BoxDrawingsLightHorizontal) + "]" + colorDefault
				tlines = append(tlines, separator)
				if comment.Resolver != nil {
					tlines = append(tlines,
						colorHeader+"Resolved by "+
							colorAuthor+tview.Escape(comment.Resolver.RealName)+
							colorDefault)
				} else {
					tlines = append(tlines,
						colorHeader+"Resolved"+colorDefault)
				}
			}
		}

		var info PatchRegion
		info.IDStart = *region + 1
		info.IDEnd = *region + (len(tlines))
		if ctxt != nil {
			info.NewFile = ctxt.NewFile
			info.NewLine = ctxt.NewLine
			info.OldFile = ctxt.OldFile
			info.OldLine = ctxt.OldLine
		}
		info.Thread = thread.ID
		info.ThreadIdx = idx

		for idx, _ := range tlines {
			*region++
			tlines[idx] = fmt.Sprintf("[\"l%d\"]%s[\"\"]%s", *region, indent, tlines[idx])
		}

		regions = append(regions, info)
		lines = append(lines, tlines...)
	}

	if len(threadidxs) > 0 {
		sep := colorSeparator + "[$" + string(tview.BoxDrawingsHeavyHorizontal) + "]" + colorDefault
		*region++
		lines = append(lines, fmt.Sprintf("[\"l%d\"]%s[\"\"]%s", *region, indent, sep))
	}

	return
}

func FormatReports(reports []string, indent string) (lines []string) {
	colorDefault := fmt.Sprintf("[%s:%s:-]",
		GetStyleColorName(ELEMENT_PRIMARY_TEXT),
		GetStyleColorName(ELEMENT_PRIMITIVE_FILL))
	colorSeparator :=
		GetStyleMarker(
			ELEMENT_SUMMARY_SEPARATOR_TEXT,
			ELEMENT_SUMMARY_SEPARATOR_FILL,
			ELEMENT_SUMMARY_SEPARATOR_ATTR)
	for _, msg := range reports {
		var tlines []string

		sep := colorSeparator + "[$" + string(tview.BoxDrawingsHeavyHorizontal) + "]" + colorDefault

		lines = append(lines, fmt.Sprintf("%s%s", indent, sep))

		rindent := "[|]"

		desc := colorfyComment(msg, rindent)
		tlines = append(tlines, desc...)
		tlines = append(tlines, rindent)

		for idx, _ := range tlines {
			tlines[idx] = fmt.Sprintf("%s%s", indent, tlines[idx])
		}

		lines = append(lines, tlines...)
	}

	return
}

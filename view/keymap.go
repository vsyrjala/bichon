// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package view

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/go-ini/ini"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config/xdg"
)

type KeymapEntry struct {
	Action  string
	Primary bool
	Default string
}

type KeymapGroup struct {
	Entries map[string]KeymapEntry
}

type Keymap struct {
	Groups map[string]KeymapGroup
}

func NewKeymap() *Keymap {
	return &Keymap{
		Groups: make(map[string]KeymapGroup),
	}
}

func runeToString(r rune) string {
	if r == ' ' {
		return "Space"
	} else if r == ';' {
		return "SemiColon"
	} else if r == '[' {
		return "BracketLeft"
	} else if r == ']' {
		return "BracketRight"
	} else if r == '=' {
		return "Equal"
	} else if r == '#' {
		return "NumberSign"
	} else {
		return string([]rune{r})
	}
}

func runeFromString(s string) rune {
	if s == "Space" {
		return ' '
	} else if s == "SemiColon" {
		return ';'
	} else if s == "BracketLeft" {
		return '['
	} else if s == "BracketRight" {
		return ']'
	} else if s == "Equal" {
		return '='
	} else if s == "NumberSign" {
		return '#'
	} else if len(s) == 1 {
		return rune(s[0])
	} else {
		return '\x00'
	}
}

func actionKeyToName(key ActionKey) (string, error) {
	var name string
	if key.Key == tcell.KeyRune {
		name = runeToString(key.Rune)
	} else {
		var ok bool
		name, ok = tcell.KeyNames[key.Key]
		if !ok {
			return "", fmt.Errorf("Unknown key name %x", key.Key)
		}
	}

	mods := key.Modifiers
	if strings.HasPrefix(name, "Ctrl-") {
		name = name[5:]
		mods |= tcell.ModCtrl
	}
	if (mods & tcell.ModAlt) == tcell.ModAlt {
		name = "alt+" + name
	}
	if (mods & tcell.ModCtrl) == tcell.ModCtrl {
		name = "ctrl+" + name
	}
	if (mods & tcell.ModShift) == tcell.ModShift {
		name = "shift+" + name
	}
	if (mods & tcell.ModMeta) == tcell.ModMeta {
		name = "meta+" + name
	}

	return name, nil
}

func actionNameToKey(name string) (ActionKey, error) {
	key := tcell.KeyNUL
	mods := tcell.ModNone

	bits := strings.Split(name, "+")
	for i := 0; i < (len(bits) - 1); i++ {
		mod := strings.ToLower(bits[i])
		if mod == "ctrl" {
			mods |= tcell.ModCtrl
		} else if mod == "alt" {
			mods |= tcell.ModAlt
		} else if mod == "shift" {
			mods |= tcell.ModShift
		} else if mod == "meta" {
			mods |= tcell.ModMeta
		} else {
			return ActionKey{}, fmt.Errorf("Unknown key modifier '%s'", mod)
		}
	}
	val := bits[len(bits)-1]

	if mods == tcell.ModCtrl {
		val = "Ctrl-" + val
		//mods = tcell.ModNone
	}
	r := runeFromString(val)
	log.Infof("Rune %s -> %x", val, r)
	if r != '\x00' {
		key = tcell.KeyRune
	} else {
		found := false
		val = strings.ToLower(val)
		for kval, name := range tcell.KeyNames {
			if strings.ToLower(name) == val {
				key = kval
				found = true
				break
			}
		}
		if !found {
			return ActionKey{}, fmt.Errorf("Unknown key name '%s'", val)
		}
	}

	k := ActionKey{
		Key:       key,
		Modifiers: mods,
		Rune:      r,
	}
	log.Infof("Convert %s -> %s", name, k.String())
	return k, nil
}

func (kmap *Keymap) UpdateActionMap(amap ActionMap) (bool, error) {
	log.Infof("Update actionmap %s", amap.GetContext())
	changed := false
	group, ok := kmap.Groups[amap.GetContext()]
	if !ok {
		// Never seen before, add all default actions
		group = KeymapGroup{
			Entries: make(map[string]KeymapEntry),
		}
		kmap.Groups[amap.GetContext()] = group
		changed = true

		for _, action := range amap.ListActions() {
			keys := amap.GetShortcuts(action)
			for _, key := range keys {
				keyname, err := actionKeyToName(key)
				if err != nil {
					return false, err
				}

				group.Entries[keyname] = KeymapEntry{
					Action: action,
				}
			}
		}

	} else {
		// Group previously loaded from config, add info
		// about defaults, so user can see if any new
		// actions have been added since their previous
		// launch
		for _, action := range amap.ListActions() {
			keys := amap.GetShortcuts(action)
			for _, key := range keys {
				keyname, err := actionKeyToName(key)
				if err != nil {
					return false, err
				}

				entry, ok := group.Entries[keyname]
				if ok {
					if entry.Action != action && entry.Default != action {
						entry.Default = action
						changed = true
						log.Infof("Action %s mapped to %s, not default %s",
							keyname, entry.Action, action)
					}
				} else {
					entry.Default = action
					changed = true
					log.Infof("Key %s has no mapping, default %s", keyname, action)
				}
				group.Entries[keyname] = entry
			}
		}
	}

	newkeys := make(map[string][]ActionKey, 0)
	for _, entry := range group.Entries {
		if entry.Action != "" {
			newkeys[entry.Action] = []ActionKey{}
		}
		if entry.Default != "" {
			newkeys[entry.Default] = []ActionKey{}
		}
	}

	for keyname, entry := range group.Entries {
		key, err := actionNameToKey(keyname)
		if err != nil {
			return false, err
		}

		if entry.Action == "" {
			continue
		}
		log.Infof("Set mapping %s -> %s (%s)", keyname, entry.Action, key.String())
		newkeys[entry.Action] = append(newkeys[entry.Action], key)
	}

	for action, keys := range newkeys {
		err := amap.SetShortcuts(action, keys...)
		if err != nil {
			return false, err
		}
	}

	return changed, nil
}

func isOldConfig(cfg *ini.File) bool {
	for _, sec := range cfg.Sections() {
		if sec.Name() == "DEFAULT" {
			continue
		}

		if sec.HasKey("shortcut-0") {
			log.Infof("Section '%s' has key 'shortcut-0', is old config", sec.Name())
			return true
		}
	}

	return false
}

func (kmap *Keymap) loadOldConfig(cfg *ini.File) error {
	log.Infof("Loading old style config")
	for _, sec := range cfg.Sections() {
		if sec.Name() == "DEFAULT" {
			continue
		}

		action := sec.Name()
		if action == "overview-page.clear" {
			action = "messages-page.clear"
		}

		keybits := strings.Split(action, ".")
		if len(keybits) != 2 {
			return fmt.Errorf("malformed keymap section name '%s'", action)
		}

		group, ok := kmap.Groups[keybits[0]]
		if !ok {
			group = KeymapGroup{
				Entries: make(map[string]KeymapEntry),
			}
			kmap.Groups[keybits[0]] = group
		}

		for _, key := range sec.Keys() {
			if !strings.HasPrefix(key.Name(), "shortcut-") {
				return fmt.Errorf("malformed key '%s' in section '%s'", key.Name(), sec.Name())
			}

			// Some runes were saved as literal values, but now
			// need encoding symbolically, so round-trip them
			name := key.String()
			r := runeFromString(name)
			if r != '\x00' {
				name = runeToString(r)
			}
			group.Entries[name] = KeymapEntry{
				Action: keybits[1],
			}
		}
	}

	return nil
}

func (kmap *Keymap) LoadConfig() error {
	keymappath := xdg.ConfigPath("keymap.ini")

	log.Infof("Loading keymap config from path %s", keymappath)
	cfg, err := ini.Load(keymappath)
	if err != nil {
		return err
	}

	if isOldConfig(cfg) {
		err = kmap.loadOldConfig(cfg)
		if err != nil {
			return err
		}

		err = kmap.SaveConfig()
		if err != nil {
			return err
		}

		return nil
	}

	for _, sec := range cfg.Sections() {
		if sec.Name() == "DEFAULT" {
			continue
		}

		group := KeymapGroup{
			Entries: make(map[string]KeymapEntry),
		}

		for _, key := range sec.Keys() {
			entry := KeymapEntry{
				Action: key.String(),
			}

			if key.Comment != "" {
				bits := strings.Split(key.Comment, " = ")
				if len(bits) == 2 {
					entry.Default = bits[1]
				}
			}

			group.Entries[key.Name()] = entry
		}

		kmap.Groups[sec.Name()] = group
	}

	return nil
}

const keynameHelp = "" +
	"Shortcut key names are either:\n" +
	" \n" +
	" * A single letter for ascii printable characters\n" +
	" * A symbolic key name for non-printable characters\n" +
	" \n" +
	"Valid symbolic names are defined in the KeyNames variable\n" +
	" \n" +
	"  https://github.com/gdamore/tcell/blob/master/key.go"

func (kmap *Keymap) SaveConfig() error {
	keymappath := xdg.ConfigPath("keymap.ini")

	cfg := ini.Empty()

	def := cfg.Section("DEFAULT")
	def.Comment = keynameHelp

	groupnames := []string{}
	for name, _ := range kmap.Groups {
		groupnames = append(groupnames, name)
	}
	sort.Strings(groupnames)
	for _, name := range groupnames {
		group := kmap.Groups[name]
		sec, _ := cfg.NewSection(name)

		shortcutnames := []string{}
		for keyname, _ := range group.Entries {
			shortcutnames = append(shortcutnames, keyname)
		}
		sort.Strings(shortcutnames)
		for _, keyname := range shortcutnames {
			entry, _ := group.Entries[keyname]
			key, _ := sec.NewKey(keyname, entry.Action)

			if entry.Default != "" {
				key.Comment = fmt.Sprintf("%s = %s", keyname, entry.Default)
			}
		}

	}

	log.Infof("Saving keymap config to path %s", keymappath)
	cfgdir := filepath.Dir(keymappath)
	err := os.MkdirAll(cfgdir, 0700)
	if err != nil {
		return err
	}

	err = cfg.SaveTo(keymappath)
	if err != nil {
		return err
	}

	return nil
}

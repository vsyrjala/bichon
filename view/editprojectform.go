// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type EditProjectFormListener interface {
	EditProjectFormCancel()
	EditProjectFormConfirm(repo model.Repo)
}

type EditProjectForm struct {
	tview.Primitive

	Form        *tview.Form
	Application *tview.Application
	Listener    EditProjectFormListener
	Directory   *tview.InputField
	Remote      *tview.InputField
	NickName    *tview.InputField
	Server      *tview.InputField
	Project     *tview.InputField
	Token       *tview.InputField
	Global      *tview.Checkbox
	State       *tview.DropDown

	Repo model.Repo
}

func NewEditProjectForm(app *tview.Application, listener EditProjectFormListener) *EditProjectForm {
	form := &EditProjectForm{
		Form:        tview.NewForm(),
		Application: app,
		Listener:    listener,
	}

	form.Primitive = Modal(form.Form, 60, 24)

	form.Directory = addInputField(form.Form, "Directory", "", 40, true)
	form.Remote = addInputField(form.Form, "Remote", "origin", 20, true)
	form.NickName = addInputField(form.Form, "Nick name", "", 40, false)
	form.Server = addInputField(form.Form, "Server", "", 20, true)
	form.Project = addInputField(form.Form, "Project", "", 20, true)
	form.Token = addInputField(form.Form, "API Token", "", 40, false)
	mask := addCheckbox(form.Form, "", "Show API token", false, false)
	form.Global = addCheckbox(form.Form, "Global", "Use token for all repos on this server", true, false)
	form.State = addDropDown(form.Form, "State", []string{
		"Active",
		"Inactive",
		"Hidden",
	}, false)

	// XXX use a unicode mask but InputField is broken
	// wrt measuring character size
	form.Token.SetMaskCharacter('*')

	mask.SetChangedFunc(func(checked bool) {
		if checked {
			form.Token.SetMaskCharacter(rune(0))
		} else {
			form.Token.SetMaskCharacter('*')
		}
	})

	form.Form.SetBorder(true).
		SetTitle("Edit existing project")

	form.Form.AddDefaultButton("Save", form.confirmButton)
	form.Form.AddButton("Cancel", form.cancelButton)
	form.Form.SetCancelFunc(form.cancelButton)

	return form
}

func (form *EditProjectForm) GetName() string {
	return "edit-project-form"
}

func (form *EditProjectForm) SetRepo(repo model.Repo) {
	form.Repo = repo
	form.Directory.SetText(form.Repo.Directory)
	form.Remote.SetText(form.Repo.Remote)
	form.NickName.SetText(form.Repo.NickName)
	form.Server.SetText(form.Repo.Server)
	form.Project.SetText(form.Repo.Project)
	form.Token.SetText(form.Repo.Token)
	form.Global.SetChecked(form.Repo.GlobalToken)
	if form.Repo.State == model.RepoStateActive {
		form.State.SetCurrentOption(0)
	} else if form.Repo.State == model.RepoStateInactive {
		form.State.SetCurrentOption(1)
	} else if form.Repo.State == model.RepoStateHidden {
		form.State.SetCurrentOption(2)
	} else {
		form.State.SetCurrentOption(0)
	}
}

func (form *EditProjectForm) clearText() {
	form.Directory.SetText("")
	form.Remote.SetText("origin")
	form.NickName.SetText("")
	form.Server.SetText("")
	form.Project.SetText("")
	form.Token.SetText("")
	form.Global.SetChecked(true)
	form.State.SetCurrentOption(0)

	form.Form.SetFocus(0)
	form.Application.SetFocus(form.Form)
}

func (form *EditProjectForm) confirmButton() {
	form.Repo.Token = form.Token.GetText()
	form.Repo.GlobalToken = form.Global.IsChecked()

	state, _ := form.State.GetCurrentOption()
	if state == -1 {
		form.Repo.State = model.RepoStateActive
	} else {
		states := []model.RepoState{
			model.RepoStateActive,
			model.RepoStateInactive,
			model.RepoStateHidden,
		}
		form.Repo.State = states[state]
	}
	nickname := form.NickName.GetText()
	if nickname == "" {
		nickname = model.GenerateRepoNickName(form.Repo.Directory, form.Repo.Remote)
	}
	form.Repo.NickName = nickname

	form.clearText()
	form.Listener.EditProjectFormConfirm(form.Repo)
}

func (form *EditProjectForm) cancelButton() {
	form.clearText()
	form.Listener.EditProjectFormCancel()
}

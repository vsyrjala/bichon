// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"strings"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/model/filter"
	"gitlab.com/bichon-project/bichon/model/sorter"
	"gitlab.com/bichon-project/tview"
)

type ReportFormListener interface {
	ReportFormConfirm(report model.Report)
	ReportFormCancel()
}

type ReportForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener ReportFormListener
	Error    *tview.TextView

	Name   *tview.InputField
	Filter *tview.InputField
	Sorter *tview.InputField

	Report model.Report
}

func (form *ReportForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.ReportFormCancel()
	form.Error.SetText("")
}

func (form *ReportForm) confirmFunc() {
	form.Form.SetFocus(0)

	name := form.Name.GetText()
	filterString := form.Filter.GetText()
	sorterString := form.Sorter.GetText()

	filterInfo := filter.BuildFilter(filterString)
	sorterInfo := sorter.BuildSorter(sorterString)
	if filterInfo.Error == nil && sorterInfo.Error == nil {
		form.Error.SetText("")
		form.Report.Name = name
		form.Report.Filter = filterString
		form.Report.Sorter = sorterString
		form.Listener.ReportFormConfirm(form.Report)
	} else {
		var errmsg []string
		if filterInfo.Error != nil {
			errmsg = append(errmsg, filterInfo.Error.Error())
		}
		if sorterInfo.Error != nil {
			errmsg = append(errmsg, sorterInfo.Error.Error())
		}
		form.Error.SetText("[red::]" + strings.Join(errmsg, "; ") + "[::]")
	}
}

func NewReportForm(listener ReportFormListener) *ReportForm {

	form := &ReportForm{
		Form:     tview.NewForm(),
		Error:    tview.NewTextView(),
		Listener: listener,
	}

	form.Error.SetDynamicColors(true)

	layout := tview.NewFlex()
	help := tview.NewTextView()
	help.SetText(helpMessage)
	layout.SetBackgroundColor(tview.Styles.PrimitiveBackgroundColor)
	layout.SetBorder(true)
	layout.SetTitle("Configure report")
	layout.SetDirection(tview.FlexRow)
	layout.AddItem(form.Form, 9, 0, true)
	layout.AddItem(form.Error, 3, 0, false)
	form.Primitive = Modal(layout, 68, 15)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Name = addInputField(form.Form, "Name", "", 60, false)
	form.Filter = addInputField(form.Form, "Filter", "", 60, false)
	form.Sorter = addInputField(form.Form, "Sort order", "", 60, false)

	form.Form.AddDefaultButton("Save", form.confirmFunc)
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *ReportForm) GetName() string {
	return "report-form"
}

func (form *ReportForm) SetReport(report model.Report) {
	form.Report = report
	form.Name.SetText(report.Name)
	form.Filter.SetText(report.Filter)
	form.Sorter.SetText(report.Sorter)
}

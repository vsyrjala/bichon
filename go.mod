module gitlab.com/bichon-project/bichon

go 1.18

require (
	github.com/alecthomas/participle/v2 v2.0.0-alpha3
	github.com/casimir/xdg-go v0.0.0-20160329195404-372ccc2180da
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
	github.com/gdamore/tcell/v2 v2.6.0
	github.com/go-ini/ini v1.62.0
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/mattn/go-runewidth v0.0.14
	github.com/mattn/go-shellwords v1.0.12
	github.com/rivo/uniseg v0.4.3
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5
	github.com/xanzy/go-gitlab v0.47.0
	github.com/zalando/go-keyring v0.1.1
	gitlab.com/bichon-project/tview v0.0.0-20230327144717-cb7f199eee46
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gopkg.in/src-d/go-git.v4 v4.13.1
)

require (
	github.com/danieljoos/wincred v1.1.0 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/godbus/dbus/v5 v5.0.3 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.4 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/smartystreets/goconvey v1.7.2 // indirect
	github.com/src-d/gcfg v1.4.0 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/oauth2 v0.0.0-20181106182150-f42d05182288 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/term v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/appengine v1.3.0 // indirect
	gopkg.in/ini.v1 v1.66.4 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
)

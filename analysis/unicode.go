// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package analysis

import (
	"fmt"
	"unicode"

	"gitlab.com/bichon-project/bichon/diff"
	"gitlab.com/bichon-project/bichon/model"
)

type unicodeCheck struct {
	Message  string
	Emoji    string
	AltEmoji string
}

type unicodeRuneCheck struct {
	unicodeCheck
	Check func(c rune) bool
}

func unicodeRuneCheckBidi(c rune) bool {
	return unicode.In(c, unicode.Bidi_Control)
}

func unicodeRuneCheckNonPrint(c rune) bool {
	return !unicode.IsGraphic(c) && c != '\t' && c != '\r'
}

var unicodeRuneChecks = []unicodeRuneCheck{
	unicodeRuneCheck{
		unicodeCheck: unicodeCheck{
			Message: "Unicode directionality control character(s) present. " +
				"Review whether this has impact on the behaviour of the code. " +
				"Consider using unicode escape sequences instead if usage is valid. " +
				"For further guidance refer to https://trojansource.codes",
			Emoji:    "💫",
			AltEmoji: "~",
		},
		Check: unicodeRuneCheckBidi,
	},
	unicodeRuneCheck{
		unicodeCheck: unicodeCheck{
			Message: "Unicode non-printable character(s) present. " +
				"Review whether this has impact on the behaviour of the code. " +
				"Consider using unicode escape sequences instead if usage is valid. " +
				"For further guidance refer to https://trojansource.codes",
			Emoji:    "👻",
			AltEmoji: "?",
		},
		Check: unicodeRuneCheckNonPrint,
	},
}

func makeContext(commit *model.Commit, diff model.Diff, hunk diff.DiffHunk, offset uint) model.CommentContext {
	context := model.CommentContext{
		BaseHash:  commit.Hash,
		StartHash: commit.Hash,
		HeadHash:  commit.Hash,
	}
	if diff.NewFile != "" && hunk.NewLine != 0 {
		context.NewFile = diff.NewFile
		context.NewLine = hunk.NewLine + offset
	}
	if diff.OldFile != "" && hunk.OldLine != 0 {
		context.OldFile = diff.OldFile
		context.OldLine = hunk.OldLine + offset
	}
	return context
}

func getCheckEmoji(check unicodeCheck, emoji bool) string {
	if emoji {
		return check.Emoji
	} else {
		return check.AltEmoji
	}
}

func getCheckMessage(check unicodeCheck, emoji bool) string {
	return fmt.Sprintf("%s %s", getCheckEmoji(check, emoji), check.Message)
}

func UnicodePatchAnalyser(commit *model.Commit, emoji bool) []PatchReport {
	reports := []PatchReport{}
	for _, d := range commit.Diffs {
		hunks, err := diff.ParseUnifiedDiffHunks(d.Content)
		if err != nil {
			continue
		}

		for _, hunk := range hunks {
			for num, line := range hunk.Lines {
				checkSeen := make([]bool, len(unicodeRuneChecks))
				for _, c := range line.Text {
					for idx, chk := range unicodeRuneChecks {
						if chk.Check(c) {
							/* Don't combine this check into the previous condition,
							 * as we don't want to fall through to the next
							 * character class check if we've already seen this one
							 */
							if !checkSeen[idx] {
								reports = append(reports,
									PatchReport{
										Message: getCheckMessage(chk.unicodeCheck, emoji),
										Context: makeContext(commit, d, hunk, uint(num)),
									})
								checkSeen[idx] = true
							}
							break
						}
					}
				}
			}
		}
	}

	return reports
}

func ShowDanger(input string, format string, emoji bool) string {
	var output string
	for _, c := range input {
		done := false
		for _, chk := range unicodeRuneChecks {
			if chk.Check(c) {
				output += fmt.Sprintf("%s{%s\\u%x}[-:-:-]", format, getCheckEmoji(chk.unicodeCheck, emoji), c)
				done = true
				break
			}
		}
		if !done {
			output += string(c)
		}
	}
	return output
}
